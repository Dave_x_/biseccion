"""
Código diseñado para calcular las raíces (o's) de una función por el método de la bisección

Definir función en otro método
Definir algoritmo de bisección en otro método

a = valor izquierda
b = valor derecho
c = prospecto de raíz iterable

Para que el algoritmo funcione se debe cumplir f(a)<f(c)<f(b) y f(a)(b)<0

OBSERVACIONES:
¿No se pueden iterar funciones cuadradas tq f(a)(b)<0?
¿A polinomios de grado mayor que dos no se le calculan todas las raíces?
- SUJETO A MEJORAS

"""


def funcion(x):
    return x**3 + x*3 + 2 # Definir la función a conveniencia
def Biseccion(a, b):

    tolerancia = 5e-8 # Definir la tolerancia
    c = (a+b)/2 # punto intermedio entre a y b

    if funcion(b)*funcion(a) < 0: # Condiciones  teorema de bolzano
        while (abs(funcion(c)) > tolerancia):  # Falta actualizar condiciones f(a)(fb) < 0 Ó f(a)(fb) > 0 ó f(a)(fb) = 0
            c = (a + b) / 2  # punto intermedio entre a y b
            if funcion(c)*funcion(a) < 0: # Condición rango de la raíz entre [a, c]
                    b=c
                    print(c)

            elif funcion(c)*funcion(a) > 0: # Condición rango de la raíz entre [c, b]
                    a=c
                    print(c)

        print(f'la raíz aproximada de la función es {c}')

    else:
        print("Valores inconrrectos para a ó b")

Biseccion(-8,10)
