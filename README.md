# Método de la Bisección para Calcular Raíces de una Función
## El Markdown o Readme fue elaborado por chatgpt y sabemos lo generoso que es con nosotros, pero en general resume lo hecho aquí.
Este código está diseñado para calcular las raíces de una función utilizando el método de la bisección.


## Función y Algoritmo de Bisección

El código consta de dos partes principales:

1. **Definición de la Función**: La función que se va a analizar se define en un método separado. En este ejemplo, la función es \( f(x) = x^3 + 3x + 2 \), pero puedes definir cualquier función a tu conveniencia.

2. **Algoritmo de Bisección**: El algoritmo de la bisección se implementa en otro método llamado `Biseccion`. Este algoritmo busca la raíz de la función dentro de un intervalo dado \([a, b]\).

## Parámetros y Condiciones

El algoritmo de la bisección toma tres parámetros:

- `a`: El valor izquierdo del intervalo.
- `b`: El valor derecho del intervalo.
- `c`: Un prospecto de raíz que es iterado para encontrar la raíz de la función.

Para que el algoritmo funcione correctamente, se deben cumplir las siguientes condiciones:

1. La función en el intervalo \([a, b]\) debe tener un cambio de signo entre \(f(a)\) y \(f(b)\), es decir, \(f(a) \cdot f(b) < 0\).
2. La función debe ser continua en el intervalo \([a, b]\).

## Observaciones

- Este código está sujeto a mejoras y optimizaciones para diferentes tipos de funciones y precisiones requeridas.
